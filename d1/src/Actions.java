public interface Actions {
    /*
        Interfaces are used to achieve abstraction. It hides away the actual implementation of the methods and simply shows a "list" or a "menu" of methods that can be done.

        Interfaces are like blueprints for your classes. Because any class that implements our interface MUST have methods in the interface
    */

    public void sleep();
    public void run();

}
