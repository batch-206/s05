public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe","+639152468596","Quezon City");

        Contact contact2 = new Contact("Jane Doe","+639162148573","Caloocan City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);
        //phonebook.contacts.add(contact1);
        //phonebook.contacts.add(contact2);

        //System.out.println(phonebook.contacts);
        //System.out.println(Arrays.deepToString(phonebook.getContacts()));


        //Define a control structure that will output a notification message in the console if the phonebook is empty,
        //else, for each contact in the phonebook, display the details.

        if(phonebook.getContacts().size() == 0 ){
            System.out.println("Your phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()){
                System.out.println("------------------------");
                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println("my home in " + contact.getAddress());
            }
        }
    }
}